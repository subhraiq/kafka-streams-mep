package GoogleFitParser;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


public class GoogleFitJsonParser
{
    public JSONArray ja = new JSONArray();
    

    @SuppressWarnings("unchecked")
    //public static void main(String[] args)
    public String jsonConverter(String val)
    {
        //JSON parser object to parse read file
        JSONParser jsonParser = new JSONParser();
        JSONObject obj =null;
        try
        {
            obj = (JSONObject) jsonParser.parse(val);
            JSONArray dataList = (JSONArray) obj.get("point");
            dataList.forEach(dat -> parseDataObject((JSONObject) dat));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return ja.toString();
    }

    //Json Value Extraction
    public void parseDataObject(JSONObject dataObj)
    {
        String startTime = (String) dataObj.get("startTimeNanos");
        JSONArray value=(JSONArray) dataObj.get("value");
        JSONObject extVal=(JSONObject) value.get(0);
        String calories=String.valueOf(extVal.get("fpVal"));
        JSONObject jo=setpatientDetails(startTime,calories);
	ja.add(jo);
    }

    //Json Object conversion and pushing it to the Json Array
    public JSONObject setpatientDetails(String strTime,String stepsValue) {
        JSONObject jo=new JSONObject();
	    jo.put("patientId", "123");
        jo.put("deviceId", "ivcliefvlku");
        jo.put("timestamp", strTime);
        jo.put("type", "calories");
        jo.put("value", stepsValue);
        return jo;
    }
}
