package StreamsApplication;

import GoogleFitParser.GoogleFitJsonParser;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.Topology;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class GoogleFitStepsStream extends Thread {
    public String con(String val){
        GoogleFitJsonParser jsc=new GoogleFitJsonParser();
        String ja=jsc.jsonConverter(val);
        return ja;
    }
    public Properties props=null;
    
    public void setProperty(Properties prop){
		props=prop;
	}
    public void run(){
        final StreamsBuilder builder = new StreamsBuilder();

        builder.stream("GoogleFit-Steps-input").flatMapValues( value -> {
            System.out.println("Inside "+value);
            return Arrays.asList(con((String)value));
        })
                .to("GoogleFit-Steps-output");
        final Topology topology = builder.build();
        final KafkaStreams streams = new KafkaStreams(topology, props);
        final CountDownLatch latch = new CountDownLatch(1);

        // attach shutdown handler to catch control-c
        Runtime.getRuntime().addShutdownHook(new Thread("streams-shutdown-hook") {
            @Override
            public void run() {
                streams.close();
                latch.countDown();
            }
        });

        try {
            streams.start();
            latch.await();
        } catch (Throwable e) {
            e.printStackTrace();
            System.out.println(e);
            System.exit(1);
        }
        System.exit(0);
    }
}
