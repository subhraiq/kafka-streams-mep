
package Kafka;


import StreamsApplication.GoogleFitStepsStream;

import java.util.Properties;

public class kafkaStart {
    public static void main(String arg[]){
        KafkaStreamsProperties kProps=new KafkaStreamsProperties();
        
	    Properties props=kProps.PropsKafkaStreams("StepsCalories");

        GoogleFitStepsStream googleSteps=new GoogleFitStepsStream();
	    googleSteps.setProperty(props);

	    googleSteps.start();
    }
}
