package Kafka;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

//Kafka Streams Initialization

public class KafkaStreamsProperties {
    String server="52.59.100.6:9092";
    String AppId="streams-pipe";
    String temp="Downloads/Temp_Kafka";
    Properties props = new Properties();
    public Properties PropsKafkaStreams(String AppId){
        props.put(StreamsConfig.APPLICATION_ID_CONFIG,AppId );
        props.put(StreamsConfig.STATE_DIR_CONFIG,temp );
        props.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG,server );
        props.put("group.id",AppId);
        props.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        props.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
        return props;
    }
}
